package com.techu.apirest.service;

import com.techu.apirest.model.ProductoModel;
import com.techu.apirest.repository.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductoService {

@Autowired
    ProductoRepository productoRepository;

    //READ
    public List<ProductoModel> findAll(){

        return productoRepository.findAll();
    }

    //READ BY ID
    public Optional<ProductoModel> findById(String id){
        return productoRepository.findById(id);

    }

    //CREATE
    public ProductoModel save(ProductoModel producto){
        return productoRepository.save(producto);
    }


    //DELETE
    public boolean delete(ProductoModel producto){
        //otra opcion diferente a delete de holamundo
        //se coloca el try poque marca en rojo al no devolver nada
        try{
            productoRepository.delete(producto);
            return true;
        } catch (Exception e){
            return false;
        }

    }

}
